package com.stellar_software.dolphin_sdk.di

import com.stellar_software.dolphin_sdk.api.RetrofitBuilder
import org.koin.dsl.module

val appModule = module {

    single { RetrofitBuilder() }

}