package com.stellar_software.dolphin_sdk

import android.app.Application
import com.stellar_software.dolphin_sdk.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class DolphinApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // Koin Android logger
            androidLogger()
            //inject Android context
            androidContext(this@DolphinApplication)
            // use modules
            modules(appModule)
        }

    }
}