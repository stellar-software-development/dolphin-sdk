package com.stellar_software.dolphin_sdk.ui

import com.stellar_software.dolphin_sdk.util.DataState

interface DataStateChangeListener {

    fun onDataStateChange(dataState: DataState<*>? )

}